# COMPAS

The repository of The COMPAS Project: a COMputational database of Polycyclic Aromatic Systems

## Content

### COMPAS-1
This folder contains the COMPAS-1x and COMPAS-1D data sets.

### COMPAS-2
This folder contains the COMPAS-2x and COMPAS-2D data sets.

## How to cite this dataset
If you use the COMPAS database or any part of it, please cite the following:

For the COMPAS-1x and COMPAS-1D datasets:
> A. Wahab, L. Pfuderer, E. Paenurk, and R. Gershoni-Poranne, The COMPAS Project: A Computational Database of Polycyclic Aromatic Systems. Phase 1: cata-condensed Polybenzenoid Hydrocarbons, J. Chem. Inf. Model. 2022, 62, 16, 3704–3713, DOI: 10.1021/acs.jcim.2c00503

For the COMPAS-2x and COMPAS-2D datasets:
> E. Mayo Yanes, S. Chakraborty, and R. Gershoni-Poranne, The COMPAS Project: A Computational Database of Polycyclic Aromatic Systems. Phase 2: cata-condensed Hetero-Polycyclic Aromatic Systems, DOI: 10.26434/chemrxiv-2023-sgh69

## Support
For support or to report any issues with the COMPAS database, please contact: porannegroup /at/ technion.ac.il

## Roadmap
Several additional instalments are planned for the COMPAS Project. The next phases are currently in development, including peri-condensed PBHs and excited state PBHs. 

## Authors and acknowledgment
The COMPAS database was conceptualized by Prof. Renana Gershoni-Poranne and is constructed under her supervision. The following people contributed to data generation, curation, and organization.

COMPAS-1:
1. Ms. Alexandra Wahab (ETH Zurich)
2. Ms. Lara Pfuderer (ETH Zurich)
3. Dr. Eno Paenurk (ETH Zurich)

COMPAS-2:
1. Mr. Eduardo Mayo Yanes (Technion)
2. Dr. Sabyasachi Chakraborty (Technion)

The invaluable assistance of the following people is gratefully acknowledged: Prof. Dr. Peter Chen, Dr. Alexandra Tsybizova. 

In addition, the financial support of the Branco Weiss Fellowship is acknowledged.

## License
The COMPAS database is provided free-of-charge and is intended to be a resource for the scientific community.
It is licensed under a CC-BY-NC-SA license. 

## Project status
The COMPAS Project is an ongoing project and the COMPAS database is under active development. Additional instalments will be reported in due course and the accompanying data will be deposited.
