-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -754.4099939666
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 64.9999905413 
   Number of Beta  Electrons                 64.9999905413 
   Total number of  Electrons               129.9999810825 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -80.4852536582 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -754.4099939666 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0369507483
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -754.4124908962
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 64.9999941210 
   Number of Beta  Electrons                 64.9999941210 
   Total number of  Electrons               129.9999882420 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -80.4241201784 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -754.4124908962 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0368548141
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -754.4134978422
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 64.9999970611 
   Number of Beta  Electrons                 64.9999970611 
   Total number of  Electrons               129.9999941221 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -80.4044118056 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -754.4134978422 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0367759803
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -754.4139184581
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 64.9999981882 
   Number of Beta  Electrons                 64.9999981882 
   Total number of  Electrons               129.9999963763 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -80.4075378843 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -754.4139184581 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0367244639
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -754.4140758556
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 64.9999977987 
   Number of Beta  Electrons                 64.9999977987 
   Total number of  Electrons               129.9999955975 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -80.4161495590 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -754.4140758556 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0367032981
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:     -754.4141175600
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 64.9999970511 
   Number of Beta  Electrons                 64.9999970511 
   Total number of  Electrons               129.9999941022 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -80.4205343435 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -754.4141175600 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0367030158
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 7
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:     -754.4141285794
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 64.9999965640 
   Number of Beta  Electrons                 64.9999965640 
   Total number of  Electrons               129.9999931281 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -80.4210543541 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -754.4141285794 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0367074081
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 8
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 8
   prop. index: 1
        SCF Energy:     -754.4141308883
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 8
   prop. index: 1
   Number of Alpha Electrons                 64.9999964602 
   Number of Beta  Electrons                 64.9999964602 
   Total number of  Electrons               129.9999929203 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -80.4199054522 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -754.4141308883 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 8
   prop. index: 1
        Van der Waals Correction:       -0.0367101899
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 9
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 9
   prop. index: 1
        SCF Energy:     -754.4141319810
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 9
   prop. index: 1
   Number of Alpha Electrons                 64.9999965790 
   Number of Beta  Electrons                 64.9999965790 
   Total number of  Electrons               129.9999931579 
   Exchange energy                            0.0000000000 
   Correlation energy                         0.0000000000 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -80.4190380873 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -754.4141319810 
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 9
   prop. index: 1
        Van der Waals Correction:       -0.0367103700
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 9
   prop. index: 1
       Filename                          : /scratch/tmp.20236549.edmayo/TM003_CHRG_0_UHF_1.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        0.2519247370
        Electronic Contribution:
                  0    
      0      -0.365264
      1      -3.465771
      2       7.308035
        Nuclear Contribution:
                  0    
      0       0.363280
      1       3.448261
      2      -7.210501
        Total Dipole moment:
                  0    
      0      -0.001983
      1      -0.017510
      2       0.097534
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    31 
    Geometry Index:     1 
    Coordinates:
               0 B     -0.011000000000   -1.864200000000    2.874000000000
               1 C     -0.350700000000   -3.084800000000    3.597900000000
               2 C     -0.780500000000   -4.093000000000    2.749700000000
               3 C     -0.866000000000   -3.918500000000    1.347400000000
               4 C     -0.513500000000   -2.716800000000    0.810800000000
               5 C     -0.036900000000   -1.526800000000    1.513700000000
               6 C      0.065600000000   -0.828400000000    0.210900000000
               7 C     -0.392200000000   -1.968300000000   -0.475800000000
               8 C     -0.538900000000   -1.985300000000   -1.840200000000
               9 C     -0.205400000000   -0.799700000000   -2.507900000000
              10 C      0.253000000000    0.351500000000   -1.854000000000
              11 C      0.401300000000    0.353200000000   -0.422900000000
              12 C      0.867500000000    1.518600000000    0.260900000000
              13 C      1.178200000000    2.642100000000   -0.423700000000
              14 C      1.050100000000    2.698300000000   -1.850000000000
              15 C      1.380100000000    3.879700000000   -2.537500000000
              16 C      1.265400000000    3.958200000000   -3.906800000000
              17 C      0.814400000000    2.848100000000   -4.624500000000
              18 C      0.485700000000    1.680600000000   -3.969800000000
              19 C      0.591700000000    1.567600000000   -2.571500000000
              20 H     -0.301200000000   -3.262800000000    4.661600000000
              21 H     -1.062200000000   -5.052000000000    3.173700000000
              22 H     -1.207900000000   -4.736500000000    0.724900000000
              23 H     -0.889000000000   -2.848600000000   -2.393400000000
              24 H     -0.314300000000   -0.792600000000   -3.586100000000
              25 H      0.963400000000    1.472600000000    1.340400000000
              26 H      1.532400000000    3.528400000000    0.092500000000
              27 H      1.729300000000    4.735500000000   -1.968900000000
              28 H      1.522300000000    4.873500000000   -4.428100000000
              29 H      0.722300000000    2.904900000000   -5.703500000000
              30 H      0.139500000000    0.840100000000   -4.559700000000
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    31 
    Geometry Index:     2 
    Coordinates:
               0 B     -0.018612815801   -1.890203501275    2.879280305398
               1 C     -0.357358276124   -3.102213603509    3.630230454151
               2 C     -0.788975630073   -4.105447316867    2.773543047274
               3 C     -0.870740661078   -3.925846426610    1.362907814477
               4 C     -0.513062198442   -2.721827189510    0.821732686401
               5 C     -0.033917305951   -1.529411171840    1.520530595885
               6 C      0.069467060112   -0.833180411326    0.206482600280
               7 C     -0.388054540459   -1.969963236657   -0.476819111147
               8 C     -0.534018171208   -1.988814299034   -1.845474869367
               9 C     -0.200878757227   -0.803053947323   -2.517726785587
              10 C      0.257547306834    0.350426295057   -1.862962503067
              11 C      0.406526481972    0.349030339995   -0.429548314276
              12 C      0.874934497939    1.518779455819    0.257682065361
              13 C      1.182778057964    2.643433344159   -0.425680680011
              14 C      1.052316836167    2.706117598780   -1.858187064335
              15 C      1.379283478122    3.892485760875   -2.545013715798
              16 C      1.262979158326    3.976427057361   -3.915985596733
              17 C      0.812353217989    2.863735086469   -4.638504830218
              18 C      0.486833664508    1.692624487939   -3.984319071492
              19 C      0.594926562607    1.575390209212   -2.583462756461
              20 H     -0.310068025567   -3.281092091949    4.704966959868
              21 H     -1.078201957095   -5.071829910479    3.196723260881
              22 H     -1.216763216812   -4.746776597195    0.733051924651
              23 H     -0.887327276788   -2.859303896587   -2.400335569234
              24 H     -0.310382813488   -0.794200608833   -3.601304237856
              25 H      0.974031811945    1.473266332821    1.344001479010
              26 H      1.538903709791    3.533743766305    0.097120316373
              27 H      1.730089260462    4.752147139375   -1.969370931941
              28 H      1.519970576441    4.900762113092   -4.437251686782
              29 H      0.717951754156    2.921951140740   -5.724826546734
              30 H      0.139968210780    0.847444080997   -4.577379238970
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    31 
    Geometry Index:     3 
    Coordinates:
               0 B     -0.027922106084   -1.917028171694    2.876667062812
               1 C     -0.364064668227   -3.117218737057    3.652931465953
               2 C     -0.795113161137   -4.113046119264    2.784295707942
               3 C     -0.872353602648   -3.926972429429    1.370622343059
               4 C     -0.510599869202   -2.721345300346    0.830643396674
               5 C     -0.029283668786   -1.524584992022    1.526239327206
               6 C      0.073813674941   -0.833708169101    0.204120704968
               7 C     -0.383582401057   -1.967479717416   -0.475685566748
               8 C     -0.529948722408   -1.987667994040   -1.846606965925
               9 C     -0.197070005975   -0.803271055648   -2.521145389547
              10 C      0.261583314607    0.351373087507   -1.865463159596
              11 C      0.411072830616    0.348766369754   -0.433076218942
              12 C      0.881291743476    1.521424133141    0.255581254129
              13 C      1.186551646608    2.645410518593   -0.427679652076
              14 C      1.053903982197    2.711041362209   -1.863440931218
              15 C      1.377566692378    3.898767584033   -2.551506664060
              16 C      1.259299423644    3.984207985958   -3.922739541678
              17 C      0.809097278024    2.868887285417   -4.646052822092
              18 C      0.487053602493    1.697263918157   -3.990114895051
              19 C      0.597635651275    1.580467131478   -2.588578212487
              20 H     -0.320223596214   -3.298478634072    4.728422323665
              21 H     -1.089801965278   -5.083254337190    3.199868681830
              22 H     -1.219113651643   -4.744783658557    0.734712064830
              23 H     -0.884048373070   -2.860567658296   -2.400033323824
              24 H     -0.305693425006   -0.791670545190   -3.605947865295
              25 H      0.983576613037    1.477477149916    1.343004935459
              26 H      1.543557418282    3.536170173286    0.096942966794
              27 H      1.728186342066    4.759134957228   -1.973877808070
              28 H      1.514614801958    4.911029750796   -4.443375388456
              29 H      0.712310549716    2.925438133708   -5.733686866582
              30 H      0.140203651413    0.848817978140   -4.580940963674
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    31 
    Geometry Index:     4 
    Coordinates:
               0 B     -0.033802474047   -1.935108552713    2.869948790440
               1 C     -0.367668287017   -3.125915466575    3.660376488779
               2 C     -0.798841020880   -4.115627416422    2.783023597907
               3 C     -0.872838516203   -3.923835398249    1.370495646551
               4 C     -0.507727513971   -2.717082819745    0.836298000083
               5 C     -0.023738816501   -1.515109093246    1.530848502580
               6 C      0.077407632392   -0.831193428230    0.205293287659
               7 C     -0.379811489611   -1.964082860981   -0.471968240648
               8 C     -0.527036995460   -1.985706266394   -1.843404054161
               9 C     -0.194977888610   -0.802909352724   -2.518730580396
              10 C      0.263800915107    0.352873517882   -1.863168223153
              11 C      0.413940261476    0.351411613741   -0.432781844214
              12 C      0.884620172742    1.525032652974    0.255235994401
              13 C      1.188460070599    2.647982919313   -0.429271574177
              14 C      1.054385986597    2.712843038449   -1.865252698889
              15 C      1.376013460305    3.899657697017   -2.555310788537
              16 C      1.256343307637    3.983568735910   -3.926183167177
              17 C      0.806474907550    2.866679316604   -4.647794091326
              18 C      0.486449657888    1.696111186507   -3.989677143918
              19 C      0.598655702665    1.581850209079   -2.588246064659
              20 H     -0.326163025527   -3.311060299221    4.733483916792
              21 H     -1.096784568077   -5.088049889616    3.190452860447
              22 H     -1.219187706420   -4.736920576349    0.729497816053
              23 H     -0.881226349178   -2.859342957672   -2.394519782119
              24 H     -0.302933714978   -0.789724226599   -3.603069562830
              25 H      0.988659495291    1.482739810330    1.341981406739
              26 H      1.545684330313    3.538785072446    0.094209532494
              27 H      1.726129631040    4.760196335203   -1.978513123465
              28 H      1.510176909421    4.909895791464   -4.447168095384
              29 H      0.708254600970    2.920811832640   -5.734845129068
              30 H      0.139781324486    0.845828875175   -4.577141676802
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    31 
    Geometry Index:     5 
    Coordinates:
               0 B     -0.037283438920   -1.942599666396    2.864642710526
               1 C     -0.370013349129   -3.129795352297    3.657966209211
               2 C     -0.799862273988   -4.117154310434    2.777176635397
               3 C     -0.871472964552   -3.920540460130    1.366627801602
               4 C     -0.504991010523   -2.712273056253    0.839602924788
               5 C     -0.019679034572   -1.505122376221    1.534862857473
               6 C      0.079569830012   -0.827747595257    0.208523028861
               7 C     -0.377777525965   -1.961282649114   -0.466934886643
               8 C     -0.526402070491   -1.984277011626   -1.838045363025
               9 C     -0.195056060167   -0.802743990866   -2.514057108074
              10 C      0.264312385151    0.354067229634   -1.859849847759
              11 C      0.415375281482    0.354778123130   -0.430572665168
              12 C      0.885710569080    1.528194094963    0.255847197091
              13 C      1.189236243378    2.650400374469   -0.430414750815
              14 C      1.054508955667    2.712837206572   -1.865289983282
              15 C      1.375661256185    3.898312416175   -2.557065121150
              16 C      1.255184594196    3.979919458618   -3.927611473824
              17 C      0.805129218140    2.862562187203   -4.647109562807
              18 C      0.485480072190    1.693117711792   -3.987540331097
              19 C      0.598461377195    1.580978962790   -2.586259619076
              20 H     -0.330360100165   -3.318497174218    4.728889000214
              21 H     -1.098659414159   -5.090850096811    3.179030432203
              22 H     -1.216364881976   -4.729597033314    0.721356384035
              23 H     -0.880603795381   -2.858259793998   -2.386982647368
              24 H     -0.303255378160   -0.789769900320   -3.597690034242
              25 H      0.990088131952    1.487072427390    1.341808507682
              26 H      1.546406154632    3.541469058540    0.091033828376
              27 H      1.725695874990    4.759011137662   -1.981970033350
              28 H      1.508426966841    4.904904174836   -4.449544163491
              29 H      0.706359067118    2.915110934976   -5.733367212914
              30 H      0.138675319940    0.842374968504   -4.572962713373
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    31 
    Geometry Index:     6 
    Coordinates:
               0 B     -0.037296241748   -1.944052813061    2.862855313366
               1 C     -0.370521860311   -3.132297489553    3.653203980222
               2 C     -0.800813717749   -4.119089362265    2.773067593741
               3 C     -0.871247515409   -3.918371880555    1.363343340286
               4 C     -0.503674687571   -2.709004581522    0.841675686953
               5 C     -0.016826562706   -1.498669090217    1.537886838945
               6 C      0.080591088142   -0.825145389009    0.211403000063
               7 C     -0.376675441560   -1.959692711170   -0.462793817369
               8 C     -0.526065151171   -1.983876774033   -1.833602109898
               9 C     -0.195322319043   -0.802949997033   -2.510484293925
              10 C      0.264153049837    0.354718303339   -1.857861610162
              11 C      0.415789731263    0.357238561738   -0.428565534727
              12 C      0.885620456855    1.530365456729    0.256504209922
              13 C      1.189138944928    2.652248344043   -0.431072924836
              14 C      1.054293801447    2.712635647314   -1.865092346315
              15 C      1.375644948099    3.897369556823   -2.557850280166
              16 C      1.255021927085    3.977355120767   -3.928347241929
              17 C      0.804867547973    2.859851187675   -4.646562568488
              18 C      0.484965450883    1.690968317133   -3.986348793539
              19 C      0.597926709560    1.579885482567   -2.585047624227
              20 H     -0.331146514825   -3.323198634779    4.723315340829
              21 H     -1.099851592140   -5.093250014403    3.172528454520
              22 H     -1.215572929085   -4.724571593127    0.714820687983
              23 H     -0.880408512511   -2.858232734800   -2.381195568933
              24 H     -0.304155110169   -0.791185882461   -3.593826065865
              25 H      0.989688528530    1.489844452664    1.342205551473
              26 H      1.546082915469    3.543795966360    0.088991403561
              27 H      1.725730413349    4.758414795475   -1.983978401253
              28 H      1.508244263785    4.901449292236   -4.451209235683
              29 H      0.706196722746    2.911766265420   -5.732536979480
              30 H      0.138121656045    0.840282197706   -4.571326005069
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    31 
    Geometry Index:     7 
    Coordinates:
               0 B     -0.037696870202   -1.943722199416    2.863154767682
               1 C     -0.371777824019   -3.134620716609    3.649794964903
               2 C     -0.801201973173   -4.121917460566    2.771216823985
               3 C     -0.870431992895   -3.917665716383    1.361340741417
               4 C     -0.502733702940   -2.707302288000    0.843451530512
               5 C     -0.015523831637   -1.494690352743    1.540346984238
               6 C      0.081137854872   -0.823406961105    0.213351073417
               7 C     -0.376282098584   -1.958557567231   -0.459781773927
               8 C     -0.526357171715   -1.983549816102   -1.830471843406
               9 C     -0.195826605034   -0.802991799476   -2.508233986136
              10 C      0.264027155965    0.355129566617   -1.856910698576
              11 C      0.416183172962    0.358735566281   -0.427284993381
              12 C      0.885785262782    1.531874257435    0.256892674612
              13 C      1.189313339947    2.653540793196   -0.431531837175
              14 C      1.054353078318    2.712690358610   -1.865244193440
              15 C      1.375917564441    3.897099568659   -2.558605759881
              16 C      1.255163642542    3.976205196029   -3.929163155637
              17 C      0.804772817011    2.858532201021   -4.646671700977
              18 C      0.484642331190    1.689933226005   -3.986037179302
              19 C      0.597653752121    1.579341652497   -2.584677165597
              20 H     -0.332692250081   -3.326053406450    4.720006037535
              21 H     -1.100045166791   -5.096440423467    3.169593829881
              22 H     -1.214012886303   -4.721914984286    0.709980954333
              23 H     -0.880851999952   -2.858296345263   -2.377341734724
              24 H     -0.305310753363   -0.792448934965   -3.591555954910
              25 H      0.989646046894    1.491689598536    1.342630526894
              26 H      1.546192849239    3.545506747079    0.087806187653
              27 H      1.726202059181    4.758514552980   -1.985458373996
              28 H      1.508475080727    4.899869381985   -4.452765519882
              29 H      0.706126627592    2.910194994063   -5.732666028162
              30 H      0.137652490905    0.839321311069   -4.571065197952
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    31 
    Geometry Index:     8 
    Coordinates:
               0 B     -0.037181376158   -1.943815533338    2.863874959109
               1 C     -0.372221040588   -3.136846647964    3.648272955108
               2 C     -0.802258829240   -4.124109609914    2.771041576842
               3 C     -0.870873685735   -3.917354362014    1.360708510440
               4 C     -0.502593949180   -2.706666374435    0.844808365076
               5 C     -0.014553666759   -1.493092298192    1.541800296591
               6 C      0.081465645778   -0.822582390147    0.214236894513
               7 C     -0.375864557845   -1.957980742196   -0.458218347422
               8 C     -0.525974558043   -1.983506879712   -1.828993689415
               9 C     -0.195605089974   -0.803097168954   -2.507286067293
              10 C      0.264144761646    0.355338565515   -1.856565644083
              11 C      0.416410901189    0.359418321516   -0.426733904753
              12 C      0.886019209719    1.532798078914    0.257041861574
              13 C      1.189396889104    2.654327034120   -0.431752128896
              14 C      1.054325409425    2.713020261780   -1.865547434012
              15 C      1.375824209836    3.897352621395   -2.559237961245
              16 C      1.255031965323    3.976103721162   -3.929864577725
              17 C      0.804682366641    2.858126510613   -4.647094751354
              18 C      0.484640340097    1.689640018618   -3.986138879125
              19 C      0.597647337822    1.579360323575   -2.584720260360
              20 H     -0.332532711171   -3.327703384617    4.718870135012
              21 H     -1.101302555705   -5.098763976042    3.168972535338
              22 H     -1.214459174191   -4.720196707411    0.707383437259
              23 H     -0.880578349485   -2.858486480772   -2.375626759616
              24 H     -0.305380753811   -0.793300881761   -3.590690351412
              25 H      0.989887708582    1.492794576454    1.342888728019
              26 H      1.546234002454    3.546546559834    0.087376260189
              27 H      1.726124105423    4.759081503909   -1.986386584772
              28 H      1.508286805917    4.899671380597   -4.453889168435
              29 H      0.706048866549    2.909611108808   -5.733205604118
              30 H      0.137709772382    0.838912850660   -4.571224401035
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    31 
    Geometry Index:     9 
    Coordinates:
               0 B     -0.037713618076   -1.943846534606    2.864287272909
               1 C     -0.373075329042   -3.137725658308    3.648174951134
               2 C     -0.802438269100   -4.125236630597    2.771318723575
               3 C     -0.870574236048   -3.917619302731    1.360694626975
               4 C     -0.502425197770   -2.706778327263    0.845370550370
               5 C     -0.014605930203   -1.492795998204    1.542243619912
               6 C      0.081587798594   -0.822451073902    0.214365116798
               7 C     -0.375853365536   -1.957760006152   -0.457851448465
               8 C     -0.526042662806   -1.983375668436   -1.828714231372
               9 C     -0.195584233548   -0.803047158861   -2.507135756767
              10 C      0.264328505076    0.355389596110   -1.856477925185
              11 C      0.416675245283    0.359456538252   -0.426689597566
              12 C      0.886422631293    1.533026031905    0.257031307147
              13 C      1.189724684328    2.654466505727   -0.431805952471
              14 C      1.054503164806    2.713211263375   -1.865751856415
              15 C      1.375844162685    3.897543223353   -2.559559284287
              16 C      1.254885603125    3.976298780160   -3.930190224623
              17 C      0.804472699355    2.858164709034   -4.647356257652
              18 C      0.484596446547    1.689718277096   -3.986219669576
              19 C      0.597808194093    1.579550929913   -2.584798108253
              20 H     -0.333257264338   -3.327310191161    4.719142520938
              21 H     -1.101456219952   -5.100011819106    3.169060865477
              22 H     -1.213876609076   -4.719978806634    0.706492879863
              23 H     -0.880643307181   -2.858404555804   -2.375391340714
              24 H     -0.305404802354   -0.793403088962   -3.590589730921
              25 H      0.990438486984    1.493068246313    1.342924948664
              26 H      1.546652632998    3.546710656329    0.087355238896
              27 H      1.726188219517    4.759372994643   -1.986757112264
              28 H      1.508025667295    4.899901544715   -4.454331822737
              29 H      0.705680932190    2.909578744006   -5.733516855801
              30 H      0.137615970861    0.838886779797   -4.571225447589
