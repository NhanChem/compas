16s CPUs detected
N_CPU: 10
Reading: ..\test_data\conf-gen\test-dataset-UFFopt.sdf
CPU time: 0
Processing ..\test_data\conf-gen\test-dataset-UFFopt.sdf
Creating directories...
CPU time: 0
Preparing xtb commands...
..\test_data\xtb-calc\test-dataset_CHRG_0\TM000\TM000_CHRG_0 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_0\TM001\TM001_CHRG_0 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_0\TM002\TM002_CHRG_0 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_0\TM003\TM003_CHRG_0 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_0\TM004\TM004_CHRG_0 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_0\TM005\TM005_CHRG_0 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_0\TM006\TM006_CHRG_0 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_0\TM007\TM007_CHRG_0 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_0\TM008\TM008_CHRG_0 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_0\TM009\TM009_CHRG_0 added to queue.
CPU time: 0
Running xTB calculations for 10 molecules ...
xtb command :"./xtb-6.5.1/bin/xtb" ..\test_data\xtb-calc\test-dataset_CHRG_0\TM000\TM000.xyz --ohess vtight --parallel 1 --gfn 1 --molden --json --namespace ..\test_data\xtb-calc\test-dataset_CHRG_0\TM000\TM000_CHRG_0 > ..\test_data\xtb-calc\test-dataset_CHRG_0\TM000\TM000_CHRG_0.xtbout.txt
CPU time: 1s
Done! Results in ..\test_data\xtb-calc\test-dataset_CHRG_0
