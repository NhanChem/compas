16s CPUs detected
N_CPU: 10
Reading: ..\test_data\conf-gen\test-dataset-UFFopt.sdf
CPU time: 0
Processing ..\test_data\conf-gen\test-dataset-UFFopt.sdf
Creating directories...
CPU time: 0
Preparing xtb commands...
..\test_data\xtb-calc\test-dataset_CHRG_-1\TM000\TM000_CHRG_-1 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_-1\TM001\TM001_CHRG_-1 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_-1\TM002\TM002_CHRG_-1 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_-1\TM003\TM003_CHRG_-1 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_-1\TM004\TM004_CHRG_-1 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_-1\TM005\TM005_CHRG_-1 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_-1\TM006\TM006_CHRG_-1 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_-1\TM007\TM007_CHRG_-1 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_-1\TM008\TM008_CHRG_-1 added to queue.
..\test_data\xtb-calc\test-dataset_CHRG_-1\TM009\TM009_CHRG_-1 added to queue.
CPU time: 0
Running xTB calculations for 10 molecules ...
xtb command :"./xtb-6.5.1/bin/xtb" ..\test_data\xtb-calc\test-dataset_CHRG_-1\TM000\TM000.xyz --ohess vtight --chrg -1 --uhf 1 --parallel 1 --gfn 1 --molden --json --namespace ..\test_data\xtb-calc\test-dataset_CHRG_-1\TM000\TM000_CHRG_-1 > ..\test_data\xtb-calc\test-dataset_CHRG_-1\TM000\TM000_CHRG_-1.xtbout.txt
CPU time: 1s
Done! Results in ..\test_data\xtb-calc\test-dataset_CHRG_-1
